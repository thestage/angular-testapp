'use strict';

angular.module('TheStageTestApp', ['ngRoute', 'ngStorage'])

// ROUTER

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/items',{templateUrl: 'partials/items.html', controller: 'Items', reloadOnSearch: false});
  $routeProvider.when('/items/:item_id',{templateUrl: 'partials/item.html', controller: 'Item'});
  $routeProvider.otherwise({redirectTo: '/items'});
}])

// SERVICES

.service('ItemsList', function($log,$localStorage,filterFilter) {
  $localStorage.$default({items: []});
  var _items = $localStorage.items;
  var _terms = '';
  var _index = -1;
  var _list = function () {
    return filterFilter(_items,_terms);
  };
  return {
    list: _list,
    add: function (item) {
      _items.push(item);
    },
    // FILTERING
    setTerms: function (terms) {
      _terms = terms;
    },
    getTerms: function () {
      return _terms;
    },
    // BROWSING
    setCurrent: function (item) {
      var returnedList = _list()
      for (var i = 0; i < returnedList.length; i++) {
        if ( returnedList[i] == item ) { _index = i; }
      }
    },
    getCurrent: function () {
      if (_index > -1) { return _list()[_index]; }
    },
    next: function () {
      if (_index < _list().length - 1) { _index++; }
    },
    previous: function () {
      if (_index > 0) { _index--; }
    }
  }
})

// CONTROLLERS

.controller('Items', ['$scope','$location','ItemsList',function($scope,$location,ItemsList) {
  // URL filter parse and set
  if ($location.search().terms) ItemsList.setTerms( $location.search().terms );
  $scope.$watch('terms',function(){
    if ($scope.terms) $location.search('terms',$scope.terms);
  })
  // controller models and actions
  $scope.terms = ItemsList.getTerms();
  $scope.items = ItemsList.list();
  $scope.add = function() {
    var item = Math.random();
    ItemsList.add( item.toString() ); 
    $scope.items = ItemsList.list();
  };
  $scope.setTerms = function() {
    ItemsList.setTerms( $scope.terms );
    $location.search('terms',$scope.terms);
    $scope.items = ItemsList.list();
  };
}])

.controller('Item', ['$scope','$routeParams','$window','ItemsList', function($scope,$routeParams,$window,ItemsList) {
  if ($routeParams.item_id) ItemsList.setCurrent( $routeParams.item_id );
  $scope.items = ItemsList.list();
  $scope.item = ItemsList.getCurrent();
  $scope.next = function () {
    ItemsList.next();
    $scope.item = ItemsList.getCurrent();
  };
  $scope.prev = function () {
    ItemsList.previous();
    $scope.item = ItemsList.getCurrent();
  };
  $scope.notFirst = function () {
    return $scope.items.length > 0 && $scope.item != $scope.items[0];
  };
  $scope.notLast = function () {
    return $scope.items.length > 0 && $scope.item != $scope.items[$scope.items.length - 1];
  }
  // item found check (if loaded from URL)
  if ( $scope.item == undefined ) $window.location.href = '#/items';
}]);
