angular-testapp
=============

AngularJS test application

Part of the [TheStage JS MVC Investigation](https://github.com/thestage/ember-testapp/wiki/TheStage-JS-MVC-Investigation)

### Features tested:

- Create and delete records
- Persist records on the client
- Filtering records through search input field or URL
- Browsing the filtered recordset in a detail view

## Requirements

Use **Bower** to install required javascript libraries. The list of dependecies is defined in bower.json

    bower install

## Run

To run on a local web server from a Mac.

- open the terminal in the project directory and run the command:

        python -m SimpleHTTPServer 8080

- open http://localhost:8080 in a browser

